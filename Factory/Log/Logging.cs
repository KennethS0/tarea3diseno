using System.IO;

namespace Logging
{
    /// <summary>
    /// Interface used to define logging methods.
    /// </summary>
    public interface ILogger
    {
        void Write(string filePath, string msg);
        void ReWrite(string filePath, string msg);
        string[] Read(string filePath);
    }

    /// <summary>
    /// Simple logger implementation that writes plain text.
    /// </summary>
    public class LogLogger : ILogger 
    {
        public LogLogger() {}

        /// <summary>
        /// This method reads a file.
        /// </summary>
        /// <param> <c>filePath</c>  File that is going to be read. </param>
        /// <returns>
        /// Lines read
        /// </returns>
        string[] ILogger.Read(string filePath)
        {
            // Use of tolerance to potential input output errors
            string[] lines = {}; 
            try {
                if (File.Exists(filePath))
                {
                    lines = File.ReadAllLines(filePath);
                }
            } catch (Exception e) {
                Console.WriteLine(e);
            }
            return lines;
        }

        /// <summary>
        /// This method writes to a file.
        /// </summary>
        /// <param> <c>filePath</c>  File in which data is going to be written </param>
        /// <param> <c>msg</c>  String with the data that is going to be written  </param>
        void ILogger.Write(string filePath, string msg)
        {
            try {
                using (var fs = new FileStream(filePath, FileMode.Append, FileAccess.Write)) 
                {
                    using (var fw = new StreamWriter(fs))
                    {                
                        fw.WriteLine(msg);
                        fw.Flush();
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }

        
        /// <summary>
        /// This method re writes a file.
        /// </summary>
        /// <param> <c>filePath</c>  File in which data is going to be written </param>
        /// <param> <c>msg</c>  String with the data that is going to be written  </param>
        void ILogger.ReWrite(string filePath, string msg)
        {
            try {
                System.IO.File.WriteAllText(filePath, msg);
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }
    };
}