namespace Vehicles
{
    /// <summary>
    /// Class <c> BicicletaMotorizadaCombustion </c> Creates a combustion bike with a motor.
    /// </summary>
    public class BicicletaMotorizadaCombustion : BicicletaMotorizada
    {
        public override string ToString()
        {
            return "Bicicleta motorizada de combustion";
        }
    }
}