namespace Vehicles
{
    /// <summary>
    /// Class <c> MotocicletaCombustion </c> Creates a combustion motorcycle.
    /// </summary>
    public class MotocicletaCombustion : Motocicleta
    {
        public override string ToString()
        {
            return "Motocicleta de combustion";
        }
    }
}