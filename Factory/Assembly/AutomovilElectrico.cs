namespace Vehicles
{
    
    /// <summary>
    /// Class <c> AutomovilElectrico </c> Creates an electric car.
    /// </summary>
    public class AutomovilElectrico : Automovil
    {
        public override string ToString()
        {
            return "Automovil electrico";
        }
    }
}