namespace Vehicles
{
    /// <summary>
    /// Class <c> MotocicletaElectrica </c> Creates an electric motorcycle.
    /// </summary>
    public class MotocicletaElectrica : Motocicleta
    {
        public override string ToString()
        {
            return "Motocicleta electrica";
        }
    }
}