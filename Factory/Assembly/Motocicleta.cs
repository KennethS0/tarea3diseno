namespace Vehicles
{
    /// <summary>
    /// Class <c> Motocicleta </c> Creates a motorcycle.
    /// </summary>
    public abstract class Motocicleta : IVehicle
    {
        protected string kind_ = "Motocicleta";

        public string GetKind()
        {
            return kind_;
        }
    }
}