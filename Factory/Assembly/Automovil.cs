
namespace Vehicles

{
    /// <summary>
    /// Class <c> Automovil </c> Creates a car .
    /// </summary>
    
    public abstract class Automovil : IVehicle
    {
        protected string kind_ = "Automovil";

        public string GetKind()
        {
            return kind_;
        }
    }
}