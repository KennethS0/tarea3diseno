using Vehicles;

namespace Assembler
{
    /// <summary>
    /// Class <c> CombustionAssembler </c> Creates different types of combustion vehicles.
    /// </summary>
    public class CombustionAssembler : IVehicleAssembler
    {
        
        /// <summary>
        /// Atributes of the class
        /// </summary>
        protected string motor_ = "Motor de combustion"; 

        /// <summary>
        /// This method returns a new Combustion Car.
        /// </summary>
        /// <returns>
        /// AutomovilCombustion.
        /// </returns>
        public Automovil CreateAutomovil()
        {
            var auto = new AutomovilCombustion();
            return auto;
        }

        /// <summary>
        /// This method returns a new Combustion Motorized Bike.
        /// </summary>
        /// <returns>
        /// BicicletaMotorizadaCombustion.
        /// </returns>
        public BicicletaMotorizada CreateBicicleta()
        {   
            var moto = new BicicletaMotorizadaCombustion();
            return moto;
        }

        /// <summary>
        /// This method returns a new Combustion Motorcycle.
        /// </summary>
        /// <returns>
        /// MotocicletaCombustion.
        /// </returns>
        public Motocicleta CreateMotocicleta()
        {
            var moto = new MotocicletaCombustion();
            return moto;;
        }
    }
}