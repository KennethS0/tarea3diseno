using Vehicles;

namespace Assembler
{
    /// <summary>
    /// Interface <c> IVehicleQA </c> Creates different vehicles.
    /// </summary>
    public interface IVehicleAssembler
    {
        Automovil CreateAutomovil();
        Motocicleta CreateMotocicleta();
        BicicletaMotorizada CreateBicicleta();
    }
}