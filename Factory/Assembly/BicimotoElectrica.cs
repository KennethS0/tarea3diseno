namespace Vehicles
{
    
    /// <summary>
    /// Class <c> BicicletaMotorizadaElectrica </c> Creates an electric Motorized bike.
    /// </summary>
    public class BicicletaMotorizadaElectrica : BicicletaMotorizada
    {
        public override string ToString()
        {
            return "Bicicleta motorizada electrica";
        }
    }
}