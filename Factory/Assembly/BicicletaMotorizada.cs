namespace Vehicles
{
    
    /// <summary>
    /// Class <c> BicicletaMotorizada </c> Creates a motorized bicicle .
    /// </summary>
    public abstract class BicicletaMotorizada : IVehicle
    {
        protected string kind_ = "Bicicleta Motorizada";

        public string GetKind()
        {
            return kind_;
        }
    }
}