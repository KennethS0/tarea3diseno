namespace Vehicles
{
    
    /// <summary>
    /// Class <c> AutomovilCombustion </c> Creates a combustion car.
    /// </summary>
    public class AutomovilCombustion : Automovil
    {
        public override string ToString()
        {
            return "Automovil de combustion";
        }
    }
}