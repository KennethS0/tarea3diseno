using Vehicles;

namespace Assembler
{
    /// <summary>
    /// Class <c> ElectricAssembler </c> Creates different electric vehicles .
    /// </summary>
    
    public class ElectricAssembler : IVehicleAssembler
    {
        protected string motor_ = "Motor Electrico"; 
   
        /// <summary>
        /// This method returns a new Electric Car.
        /// </summary>
        /// <returns>
        /// AutomovilElectrico.
        /// </returns>
        public Automovil CreateAutomovil()
        {
            var auto = new AutomovilElectrico();
            return auto;
        }

        /// <summary>
        /// This method returns a new Electric Motorized Bike.
        /// </summary>
        /// <returns>
        /// BicicletaMotorizadaElectrica.
        /// </returns>
        public BicicletaMotorizada CreateBicicleta()
        {   
            var moto = new BicicletaMotorizadaElectrica();
            return moto;
        }

        /// <summary>
        /// This method returns a new Electric Motorcycle.
        /// </summary>
        /// <returns>
        /// MotocicletaElectrica.
        /// </returns>
        public Motocicleta CreateMotocicleta()
        {
            var moto = new MotocicletaElectrica();
            return moto;;
        }
    }
}