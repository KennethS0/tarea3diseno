﻿using Test;
using Assembler;
using Factories;

// Creates the Electric Factory
IFactoryCreator fc = new ElectricFactoryCreator();
var electricFact = fc.createFactory();

// Creates the Combustion Factory
fc = new CombustionFactoryCreator();
var combustionFact = fc.createFactory();

var v1 = electricFact.AssembleVehicle(VehicleType.AUTOMOVIL);
var v2 = combustionFact.AssembleVehicle(VehicleType.MOTOCICLETA);
var v3 = combustionFact.AssembleVehicle(VehicleType.BICIMOTO);
var v4 = electricFact.AssembleVehicle(VehicleType.MOTOCICLETA);
