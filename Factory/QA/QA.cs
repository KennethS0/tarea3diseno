using Assembler;
using Vehicles;
using Logging;
using System.Collections.Generic;

namespace Test
{
    /// <summary>
    /// Class <c> QA </c> Creates a class with singleton to test vehicles.
    /// </summary>
    public class QA{

        ///  instance of the tester as null
        private static QA tester_ = null;
        /// List of results of tests.
        protected List<String> testresults_;
        /// Counter of tests 
        private int test_count_; 
        /// The logger creates the files with the logs.
        protected ILogger logger_;

        /// <summary>
        /// Method <c> QA </c> is a private contructor due to singleton method.
        /// </summary>
        private QA(){
            this.test_count_ = 0;
            this.testresults_ = new List<String>();
            this.logger_ = new LogLogger();
        }

        /// <summary>
        /// Method <c> GetInstance </c> gets the existing instance of QA and if it's null, it creates it first.
        /// </summary>

        public static QA GetInstance()
        {
            if(tester_ == null)
            {
                tester_ = new QA();
            }

            return tester_;
        }

        /// <summary>
        /// Method <c> SaveTestResult </c> Saves the results and once it has already run 5 tests it logs them.
        /// </summary>

        public void SaveTestResult(String test) {
            Console.WriteLine($"Total Tests tested by now: {this.test_count_}");
            this.test_count_++;

            this.testresults_.Add(test);
            if (this.testresults_.Count == 5) {
                String file = $"Pruebas_{DateTime.Now.ToString("yyyyMMdd")}_{DateTime.Now.Hour}_{DateTime.Now.Minute}.log";
                this.logger_.Write(file, this.testresults_[0]);
                this.logger_.Write(file, this.testresults_[1]);
                this.logger_.Write(file, this.testresults_[2]);
                this.logger_.Write(file, this.testresults_[3]);
                this.logger_.Write(file, this.testresults_[4]);
                this.testresults_.Clear();
            }
        }

        /// <summary>
        /// Method <c> TestAcelerar </c> Test the aceleration of a given vehicle.
        /// </summary>

        public string TestAcelerar(IVehicle vehicle)
        {
            string result = $"Probando aceleracion en {vehicle.ToString()}: PASS";
            Console.WriteLine(result);
            this.SaveTestResult(result);
            return result;
        }

        /// <summary>
        /// Method <c> TestAcelerar </c> Test the capability to move forward of a given vehicle.
        /// </summary>

        public string TestAvanzar(IVehicle vehicle)
        {
            string result = $"Probando avanzar en {vehicle.ToString()}: PASS";
            Console.WriteLine(result);
            this.SaveTestResult(result);
            return result;
        }

        /// <summary>
        /// Method <c> TestAcelerar </c> Test the brakes of a given vehicle.
        /// </summary>

        public string TestFrenar(IVehicle vehicle)
        {
            string result = $"Probando freno en {vehicle.ToString()}: PASS";
            Console.WriteLine(result);
            this.SaveTestResult(result);
            return result;
        }

        /// <summary>
        /// Method <c> TestAcelerar </c> Test the capability to go backwards of a given vehicle.
        /// </summary>

        public string TestRetroceder(IVehicle vehicle)
        {
            string result = $"Probando retroceso en {vehicle.ToString()}: PASS";
            Console.WriteLine(result);
            this.SaveTestResult(result);
            return result;
        }
    }
}