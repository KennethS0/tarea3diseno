namespace Factories
{
    /// <summary>
    /// Interface <c> IFactoryCreator </c> Implementation of the factory design pattern.
    /// </summary>
    public interface IFactoryCreator {
        public Factory createFactory();
    }
}