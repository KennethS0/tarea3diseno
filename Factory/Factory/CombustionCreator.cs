namespace Factories
{
    /// <summary>
    /// Class <c> CombustionFactoryCreator </c> This class is used to follow the design pattern Factory Method.
    /// </summary>
    public class CombustionFactoryCreator : IFactoryCreator {

        public CombustionFactoryCreator() {}

        public Factory createFactory() {
            return new CombustionFactory();
        }
    }
}