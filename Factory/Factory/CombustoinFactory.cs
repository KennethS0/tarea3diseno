using Test;
using Assembler;
using Vehicles;
using Logging;

namespace Factories
{
    /// <summary>
    /// Class <c> CombustionFactory </c> Creates a combustion factory.
    /// </summary>
    public class CombustionFactory : Factory
    {
        public CombustionFactory() {
            this.assembler_ = new CombustionAssembler();
        }   
    }
}