using Test;
using Assembler;
using Vehicles;
using Logging;

namespace Factories
{
    /// <summary>
    /// Class <c> ElectricFactory </c> Creates a combustion factory.
    /// </summary>
    public class ElectricFactory : Factory
    {
        public ElectricFactory() {
            this.assembler_ = new ElectricAssembler();
        }
    }
}