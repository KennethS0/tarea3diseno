using Test;
using Assembler;
using Vehicles;
using Logging;

namespace Factories
{
    /// <summary>
    /// Enums of car, bicicle and motorcicle
    /// </summary>
    public enum VehicleType {
        AUTOMOVIL,
        MOTOCICLETA,
        BICIMOTO
    }
    /// <summary>
    /// Class <c> Factory </c> Creates a vehicle and test it.
    /// </summary>
    public abstract class Factory
    {
        protected QA qa = QA.GetInstance();
        protected IVehicleAssembler assembler_;

        /// <summary>
        /// This method creates a new vehicle.
        /// </summary>
        /// <param> <c>vt</c>  Enum that indicates the type of vehicle. </param>
        /// <returns>
        /// New Vehicle
        /// </returns>
        public IVehicle CreateVehicle(VehicleType vt) {
            IVehicle v = null;
            if (vt == VehicleType.AUTOMOVIL) {
                v = this.assembler_.CreateAutomovil();
            }
            else if (vt == VehicleType.MOTOCICLETA) {
                v = this.assembler_.CreateMotocicleta();
            }
            else if (vt == VehicleType.BICIMOTO) {
                v = this.assembler_.CreateBicicleta();
            }
            return v;
        }

        /// <summary>
        /// This method creates a vehicle and runs all of the tests.
        /// </summary>
        /// <param> <c>vt</c>  Enum that indicates the type of vehicle. </param>
        /// <returns>
        /// New Vehicle
        /// </returns>
        public IVehicle AssembleVehicle(VehicleType vt) {
            IVehicle v = this.CreateVehicle(vt);

            this.qa.TestAcelerar(v);
            this.qa.TestAvanzar(v);
            this.qa.TestFrenar(v);
            this.qa.TestRetroceder(v);

            return v;
        }

    }
}