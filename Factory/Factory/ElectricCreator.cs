namespace Factories
{
    /// <summary>
    /// Class <c> ElectricFactoryCreator </c> This class is used to follow the design pattern Factory Method.
    /// </summary>
    public class ElectricFactoryCreator : IFactoryCreator {

        public ElectricFactoryCreator() {}

        public Factory createFactory() {
            return new ElectricFactory();
        }
    }
}